/**
 * BLOCK: Section
 */

import './style.scss';
import './editor.scss';
import Inspector from './inspector';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

const blockAttributes = {
	sectionType: {
		type: 'string',
		default: 'container',
	},
	align: {
		type: 'string',
		default: 'full',
	},
	backgroundColor: {
		type: 'string',
	},
	backgroundImage: {
		type: 'string',
	},
	backgroundOpacity: {
		type: 'number',
		default: 0,
	},
};

registerBlockType( 'investis-blocks/section', {
	title: __( 'Section', 'investis-blocks' ),
	description: __( 'Section element block with custom background image or color. Used to control other block alignments', 'investis-blocks' ),
	icon: 'align-wide',
	category: 'design',
	keywords: [
		__( 'section', 'investis-blocks' ),
		__( 'Investis Blocks', 'investis-blocks' ),
		__( 'Investis', 'investis-blocks' ),
	],
	attributes: blockAttributes,

	getEditWrapperProps() {
		return { 'data-align': 'full' };
	},

	edit: function( props ) {
		const { attributes, className } = props;

		const wrapperStyle = {
			backgroundColor: attributes.backgroundColor,
			backgroundImage: attributes.backgroundImage && 'url(' + attributes.backgroundImage + ')',
		};

		const classes = classnames(
			className,
			dimRatioToClass( attributes.backgroundOpacity ),
			{
				'has-background-dim': attributes.backgroundOpacity !== 0,
			},
			attributes.sectionType,
		);
		
		const wrapperClasses = classnames(
			className,
			'section-inner',
			attributes.align,
		);

		return (
			<Fragment>
				<Inspector { ...props } />
				<section style={ wrapperStyle } className={ classes }>
					<div className={ wrapperClasses }>
						<InnerBlocks />
					</div>
				</section>
			</Fragment>
		);
	},

	save: function( props ) {
		const { attributes, className } = props;

		const wrapperStyle = {
			backgroundColor: attributes.backgroundColor,
			backgroundImage: attributes.backgroundImage && 'url(' + attributes.backgroundImage + ')',
		};

		const classes = classnames(
			className,
			attributes.sectionType,
			dimRatioToClass( attributes.backgroundOpacity ),
			{
				'has-background-dim': attributes.backgroundOpacity !== 0,
			},
		);
		
		const wrapperClasses = classnames(
			className,
			'section-inner',
			attributes.align,
		);

		return (
			<section style={ wrapperStyle } className={ classes }>
				<div className={ wrapperClasses }>
						<InnerBlocks.Content />
				</div>
			</section>
		);
	},

} );

function dimRatioToClass( ratio ) {
	return ( ratio === 0 ) ?
		null :
		'has-background-dim-' + ( 10 * Math.round( ratio / 10 ) );
}
