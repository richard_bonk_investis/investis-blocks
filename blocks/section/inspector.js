/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Component } = wp.element;
const { InspectorControls, PanelColorSettings, MediaUpload } = wp.blockEditor;
const { PanelBody, RangeControl, SelectControl, ToggleControl, Button, Spinner } = wp.components;

/**
 * Inspector controls
 */
export default class Inspector extends Component {

	render() {
		const { attributes, setAttributes } = this.props;

		return (
			<InspectorControls key="inspector">
				<PanelBody title={ __( 'Section', 'investis-blocks' ) } >
					<SelectControl
						label={ __( 'Section Type', 'investis-blocks' ) }
						value={ attributes.sectionType }
						onChange={ ( sectionType ) => setAttributes( { sectionType } ) }
						options={ [
							{ value: 'container', label: __( 'Container', 'investis-blocks' ) },
							{ value: 'container-fluid', label: __( 'Container Fluid', 'investis-blocks' ) },
						] }
					/>
				</PanelBody>
				<PanelBody title={ __( 'Section Inner', 'investis-blocks' ) } >
					<SelectControl
						label={ __( 'Alignment', 'investis-blocks' ) }
						value={ attributes.position }
						onChange={ ( position ) => setAttributes( { position } ) }
						options={ [
							{ value: 'wide', label: __( 'Wide', 'investis-blocks' ) },
							{ value: 'full', label: __( 'Full', 'investis-blocks' ) },
							{ value: 'extra', label: __( 'Extra', 'investis-blocks' ) },
						] }
					/>
				</PanelBody>
				<PanelBody title={ __( 'Background Image', 'investis-blocks' ) } >
					{ !! attributes.backgroundImage &&
						<MediaUpload
							title={ __( 'Set featured image', 'investis-blocks' ) }
							onSelect={ ( value ) => setAttributes( { backgroundImage: value.url } ) }
							type="image"
							modalClass="editor-post-featured-image__media-modal"
							render={ ( { open } ) => (
								<Button className="editor-post-featured-image__preview" onClick={ open }>
									{ attributes.backgroundImage &&
										<img src={ attributes.backgroundImage } alt={ __( 'Featured image', 'investis-blocks' ) } />
									}
									{ ! attributes.backgroundImage && <Spinner /> }
								</Button>
							) }
						/>
					}
					{ ! attributes.backgroundImage &&
						<MediaUpload
							onSelect={ ( value ) => setAttributes( { backgroundImage: value.url } ) }
							type="image"
							value={ attributes.backgroundImage }
							render={ ( { open } ) => (
								<Button className="button" onClick={ open }>
									{ __( 'Open Media Library', 'investis-blocks' ) }
								</Button>
							) }
						/>
					}
					{ !! attributes.backgroundImage &&
						<Button onClick={ () => setAttributes( { backgroundImage: undefined } ) } isLink isDestructive>
							{ __( 'Remove Image', 'investis-blocks' ) }
						</Button>
					}
				</PanelBody>
				<PanelBody title={ __( 'Background Opacity', 'investis-blocks' ) } >
					<RangeControl
						value={ attributes.backgroundOpacity }
						label={ __( 'Background Opacity (%)', 'investis-blocks' ) }
						onChange={ ( backgroundOpacity ) => setAttributes( { backgroundOpacity } ) }
						min={ 0 }
						max={ 100 }
						step={ 10 }
					/>
				</PanelBody>
				<PanelColorSettings
					title={ __( 'Color Settings' ) }
					colorSettings={ [
						{
							value: attributes.backgroundColor,
							onChange: ( backgroundColor ) => setAttributes( { backgroundColor } ),
							label: __( 'Background Color', 'investis-blocks' ),
						},
					] }
				>
				</PanelColorSettings>
			</InspectorControls>
		);
	}

}
