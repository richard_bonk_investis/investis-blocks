<?php
/**
 * Plugin Name:       Investis Blocks
 * Description:       Gutenberg Block Library
 * Requires at least: 5.8
 * Requires PHP:      7.0
 * Version:           1.0.0
 * Author:            Investis Digital
 * Text Domain:       investis-blocks
 *
 * @package           create-block
 */

function create_investis_blocks_init()
{

	$blocks = array(
		'section/'
	);

	foreach($blocks as $block) {
		register_block_type(plugin_dir_path(__FILE__) . 'blocks/'. $block);
	}

	
}
add_action('init', 'create_investis_blocks_init');
